# Doorkeeper Devise+Omniauth Client


This app is an example of OAuth2 client. It was built in order to test
It uses [rails](http://github.com/rails/rails/), [devise](http://github.com/plataformatec/devise)
and [omniauth](http://github.com/intridea/omniauth) gems. OAuth2
strategy is build on top of [abstract OAuth2 strategy for OmniAuth](https://github.com/intridea/omniauth-oauth2)


## Installation & Configuration

If you want to run the application by yourself here are the steps for
you.


Install all the gems

    bundle install

And migrate the database

    bundle exec rake db:migrate

At this point the application should be ready to run, but it won't
communicate correctly with the provider. You need to set up environment
variables to indicate the oauth2 provider values. In your
`.env` file, setup these variables

    DOORKEEPER_APP_ID = <client_id>
    DOORKEEPER_APP_SECRET = <secret>
    DOORKEEPER_APP_URL=http://localhost:3000/?tenant=demo2
    DOORKEEPER_APP_USERINFO_ENDPOINT=http://localhost:3000/oauth/userinfo/?tenant=demo2
    DOORKEEPER_APP_REDIRECT_CALLBACK=http://localhost:3001/users/auth/doorkeeper/callback

