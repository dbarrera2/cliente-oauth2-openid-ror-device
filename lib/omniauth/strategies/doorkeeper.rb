module OmniAuth
  module Strategies
    class Doorkeeper < OmniAuth::Strategies::OAuth2
      option :name, :doorkeeper

      option :client_options,
             site: ENV["DOORKEEPER_APP_URL"],
             authorize_path: "/oauth/authorize"

      uid do
        raw_info["id"]
      end

      info do
        {
          email: raw_info["email"]
        }
      end

      def raw_info
        @raw_info ||= access_token.get(ENV["DOORKEEPER_APP_USERINFO_ENDPOINT"]).parsed
      end

      # def request_phase
      #   set_client_config
      #   super
      # end

      # def callback_phase
      #   set_client_config
      #   super
      # end

      # def callback_url
      #   full_host + script_name + callback_path
      # end

      private
        def set_client_config
          # setear las configuraciones usando el método options permite que sólo se configuren
          # al iniciar la app. Como tenemos tenants lo tenemos que hacer dinámico en cada
          # request
          # @options[:client_id] = ENV["DOORKEEPER_APP_ID"]
          # @options[:client_secret] = ENV["DOORKEEPER_APP_SECRET"]
          # @options[:client_options][:authorize_url] = ENV["DOORKEEPER_APP_AUTHORIZE_URL"]
          # @options[:client_options][:token_url] = ENV["DOORKEEPER_APP_TOKEN_URL"]
          # @options[:client_options][:userinfo_endpoint] = ENV["DOORKEEPER_APP_USERINFO_ENDPOINT"]
        end
    end
  end
end
